from django.contrib import admin

from .models import Article, Publication, Section, PublicationArticle, Author

class PublicationArticleInline(admin.TabularInline):
    model = PublicationArticle
    extra = 0

# Register your models here.
class ArticleAdmin(admin.ModelAdmin):
    inlines = [PublicationArticleInline]

admin.site.register(Article, ArticleAdmin)
admin.site.register(Publication)
admin.site.register(Section)
admin.site.register(Author)
