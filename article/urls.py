from django.conf.urls import url

from article.views import *

urlpatterns = [
    url(r'^$', ArticleListView.as_view(), name='article-list'),
    url(r'^(?P<pk>\d+)/$', ArticleDetailView.as_view(), name='article-detail')
]
