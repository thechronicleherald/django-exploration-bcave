from django.db import models
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType

# Create your models here.
class Publication(models.Model):
    name = models.CharField(max_length=200)
    editor_permission = models.ForeignKey('auth.Permission', related_name='editor_permission', null=True, blank=True)
    admin_permission = models.ForeignKey('auth.Permission', related_name='admin_permission', null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.pk:
            insert = True

        super(Publication, self).save(*args, **kwargs)

        if insert:
            # Create permissions and groups for this publication
            content_type = ContentType.objects.get_for_model(Publication)

            # Publication Editor
            self.editor_permission = Permission.objects.get_or_create(
                codename = '_'.join([self.name, "editor"]),
                name = 'Can edit %s stories' % self.name,
                content_type = content_type,
            )[0]
            editor_group = Group.objects.get_or_create(name="%s Editor" % self.name)[0]
            editor_group.permissions.add(self.editor_permission)

            # Publication Admin
            self.admin_permission = Permission.objects.get_or_create(
                codename = '_'.join([self.name, "admin"]),
                name = 'Can admin the %s publication' % self.name,
                content_type = content_type,
            )[0]
            admin_group = Group.objects.get_or_create(name="%s Admin" % self.name)[0]
            admin_group.permissions.add(self.admin_permission)

            # Update Super Admin
            audience_manager = Group.objects.get_or_create(name="Audience Manager")[0]
            audience_manager.permissions.add(self.editor_permission, self.admin_permission)

            # Save changes
            super(Publication, self).save(*args, **kwargs)





class Section(models.Model):
    name = models.CharField(max_length=200)
    parent = models.ForeignKey('self', null=True, blank=True)
    publication = models.ForeignKey('Publication', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        # Ensure parent is of the same Publication
        if(self.publication != self.parent.publication):
            raise ValueError("Section and parent section do not have the same Publication")
        super(Section, self).save(*args, **kwargs)

class Article(models.Model):
    headline = models.CharField(max_length=200)
    subhead  = models.CharField(max_length=200)
    body = models.TextField()
    publications = models.ManyToManyField(
        'Publication',
        through='PublicationArticle',
    )
    authors = models.ManyToManyField("Author")
    main_image = models.ImageField(null=True)

    @property
    def slug(self):
        return '-'.join([self.headline.lower().replace(" ","-"), str(self.id)])

    def __str__(self):
        return self.headline


class PublicationArticle(models.Model):
    article = models.ForeignKey('Article', on_delete=models.CASCADE)
    publication = models.ForeignKey('Publication', on_delete=models.CASCADE)
    sections = models.ManyToManyField('Section')

    def __str__(self):
        return ':'.join([self.publication.name, self.article.headline])

    def save(PublicationArticle, *args, **kwargs):
        # Ensure all these sections come from the correct Publication
        for section in self.sections.all():
            if(section.publication != self.publication):
                raise ValueError("PublicationArticle relationship has sections of a different Publication")

        super(PublicationArticle, self).save(*args, **kwargs)

class Author(models.Model):
    name = models.CharField(max_length=200)
    bio = models.TextField()

    def __str__(self):
        return self.name
