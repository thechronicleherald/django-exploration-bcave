from django.shortcuts import render
from django.views.generic import DetailView, ListView, FormView
from django.contrib.auth.forms import AuthenticationForm
from django.core.urlresolvers import reverse_lazy


from article.models import Article

class ArticleListView(ListView):
    model = Article

class ArticleDetailView(DetailView):
    model = Article
