from django.contrib import admin
from .models import CQueue, CQueueItems
# Register your models here.
class CQueueItemsAdmin(admin.ModelAdmin):
    readonly_fields=('item_ct',)

admin.site.register(CQueue)
admin.site.register(CQueueItems, CQueueItemsAdmin)
