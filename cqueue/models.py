from django.db import models
from gm2m import GM2MField
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType

# Create your models here
class CQueue(models.Model):
    name = models.CharField(max_length=50)
    items_ct = models.ForeignKey(ContentType, related_name="items_ct", null=True, blank=True)
    items = GM2MField(through="CQueueItems")

    def __str__(self):
        return self.name

class CQueueItems(models.Model):
    cqueue = models.ForeignKey(CQueue)
    item = GenericForeignKey(ct_field='item_ct', fk_field='item_fk')
    item_ct = models.ForeignKey(ContentType)
    item_fk = models.CharField(max_length=255)
    position = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return "%s in %s" % (self.item, self.cqueue)

    def save(self, *args, **kwargs):
        self.item_ct = self.cqueue.items_ct
        super(CQueueItems, self).save(*args, **kwargs)
