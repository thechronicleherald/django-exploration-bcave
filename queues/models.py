from django.db import models
from django.contrib.contenttypes.fields import GenericRelation

# Create your models here.
class Queue(models.Model):
    name = models.CharField(max_length=200)
    items = models.ManyToManyField('QueueItems')

class QueueItems(models.Model):
    content = models.GenericForeignKey()
    posistion = models.IntegerField
